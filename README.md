# star test

## One-Sentence-Pitch

Unbleachable beads for diagnosing issues in the detection path of confocal microscopes


## How does it work?

I have developed a portable constant flux light source, that consists of three color LEDs which are mounted behind a first surface mirror and a constant current driver which is battery powered. Low quality commercial first surface mirrors all contain tiny hole defects. I am making use of these holes and illuminate them by the LED in this setup. Schematically it looks like this:

<img src="msetupCAD.jpg" width=30% />

In the microscope the light that goes through the defects in the mirror looks like a constellation of stars. Hence, selected holes can easily be identified, so that you can always use the same star that has an immutable brightness for your tests. Unlike a bead sample these stars do not bleach and enable multiple microscope quality assessment assays:   
 
1. Evaluation of the (detection) point spread function. 
2. Pinhole size measurement. This is shown here (Multi-track of three different pinhole sizes shown as 3 different colors):

 <img src="pinholeSize.jpg" width=30% />
 
(Yes, the pinhole of most commercial systems is a square.)

3. Alignment of the detection path of a confocal microscope. 
4. Evaluation of the detection efficiency (over time and between systems). 
5. Optical performance measurements such as NA of an objective over the field
6. ...

Here is a picture of the current device:

 <img src="device.jpg" width=30% />

## Status

The device proved very successful in practice to diagnose performance issue of confocal microscopes. I am currently working on the manual. Further, I am working with electronics engineers to develop a very accurate constant current LED driver circuit board in small series so you can get your own star-test. 

## Acknowledgment

This project has been made possible in part by grant number 2020-225401 from the Chan Zuckerberg Initiative DAF, an advised fund of Silicon Valley Community Foundation.